class AddressDetail < ApplicationRecord
  belongs_to :contact_name
  validates :address_text,:city_name,:state_name,:country_name, presence: true
end
