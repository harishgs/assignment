class PhoneNumber < ApplicationRecord
  belongs_to :contact_name
  validates :contact_number, presence: true
end
