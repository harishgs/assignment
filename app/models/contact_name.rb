class ContactName < ApplicationRecord
  has_many :address_details, dependent: :destroy
  has_many :phone_numbers, dependent: :destroy
  accepts_nested_attributes_for :address_details ,allow_destroy: true
  accepts_nested_attributes_for :phone_numbers, allow_destroy: true
  validates :firstName, :lastName, :email, presence: true
  validates_associated :address_details,presence: true
  validates_associated :phone_numbers,presence: true
end
