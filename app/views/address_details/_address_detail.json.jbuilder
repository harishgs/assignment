json.extract! address_detail, :id, :address1, :address2, :address3, :pincode, :created_at, :updated_at
json.url address_detail_url(address_detail, format: :json)
