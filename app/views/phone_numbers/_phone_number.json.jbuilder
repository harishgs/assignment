json.extract! phone_number, :id, :contact_number, :created_at, :updated_at
json.url phone_number_url(phone_number, format: :json)
