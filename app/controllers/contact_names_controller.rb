class ContactNamesController < ApplicationController
  before_action :set_contact_name, only: [:show, :edit, :update, :destroy]

  # GET /contact_names
  # GET /contact_names.json
  def index
    @contact_names = ContactName.all
  end

  # GET /contact_names/1
  # GET /contact_names/1.json
  def show
    if @contact_name.blank?
      redirect_to action: 'index'
    else
      @contact_name = ContactName.where("id=?",@contact_name.id).includes(:address_details,:phone_numbers)
    end
  end

  # GET /contact_names/new
  def new
    @contact_name = ContactName.new
    @contact_name.address_details.new
    @contact_name.phone_numbers.new
  end

  # GET /contact_names/1/edit
  def edit
  end

  # POST /contact_names
  # POST /contact_names.json
  def create
    @contact_name = ContactName.new(contact_name_params)
    respond_to do |format|
      if @contact_name.save
        format.html { redirect_to @contact_name, notice: 'Contact name was successfully created.' }
        format.json { render :show, status: :created, location: @contact_name }
      else
        format.html { render :new }
        format.json { render json: @contact_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contact_names/1
  # PATCH/PUT /contact_names/1.json
  def update
    respond_to do |format|
      if @contact_name.update(contact_name_params)
        format.html { redirect_to @contact_name, notice: 'Contact name was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact_name }
      else
        format.html { render :edit }
        format.json { render json: @contact_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contact_names/1
  # DELETE /contact_names/1.json
  def destroy
    @contact_name.destroy
    respond_to do |format|
      format.html { redirect_to contact_names_url, notice: 'Contact name was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact_name
      if params[:id] == "contact_names"
      @contact_name =[]
      else  
      @contact_name = ContactName.find(params[:id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_name_params
      params.require(:contact_name).permit(:firstName, :lastName, :email,address_details_attributes:[:id,:address_text,:pincode,:city_name,:state_name,:country_name,:_destroy],phone_numbers_attributes:[:id,:contact_number,:_destroy])
    end
end
