class HomeController < ApplicationController
  def set_breadcrumbs
    super
    add_breadcrumb Faker::StarWars.character, '#controllerbase'
  end

  def index
    if user_signed_in?
      add_breadcrumb Faker::StarWars.character, '#view'
    else
      redirect_to new_user_session_path
    end
    
  end
end
