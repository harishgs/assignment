class RemoveColumnsAddress2AndAddress3FromAddressDetails < ActiveRecord::Migration[5.1]
  def change
    remove_column :address_details, :address2
    remove_column :address_details, :address3
    rename_column :address_details, :address, :address_text
  end
end
