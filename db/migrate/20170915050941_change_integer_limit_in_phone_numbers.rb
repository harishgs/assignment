class ChangeIntegerLimitInPhoneNumbers < ActiveRecord::Migration[5.1]
  def change
      change_column :phone_numbers, :contact_number, :integer, limit: 8
  end
end
