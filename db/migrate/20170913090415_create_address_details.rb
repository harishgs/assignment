class CreateAddressDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :address_details do |t|
      t.string :address1
      t.string :address2
      t.string :address3
      t.integer :pincode

      t.timestamps
    end
  end
end
