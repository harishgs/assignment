class AddColumnAddressAreaAndCityAndStateAndCountryToAddressDetails < ActiveRecord::Migration[5.1]
  def change
    add_column :address_details, :city_name,:string, limit: 30
    add_column :address_details, :state_name,:string, limit: 30
    add_column :address_details, :country_name, :string, limit: 30
    add_column :address_details, :address, :text, limit:225
  end
end
