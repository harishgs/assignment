class CreateContactNames < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_names do |t|
      t.string :firstName
      t.string :lastName
      t.string :email
      
      t.timestamps
    end
  end
end
