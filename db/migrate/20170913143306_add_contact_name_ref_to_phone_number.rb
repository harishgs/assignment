class AddContactNameRefToPhoneNumber < ActiveRecord::Migration[5.1]
  def change
    add_reference :phone_numbers, :contact_name, foreign_key: true
  end
end
