class AddAddressDetailRefToContactName < ActiveRecord::Migration[5.1]
  def change
    add_reference :address_details,:contact_name , foreign_key: true
  end
end
