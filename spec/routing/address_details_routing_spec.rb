require "rails_helper"

RSpec.describe AddressDetailsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/address_details").to route_to("address_details#index")
    end

    it "routes to #new" do
      expect(:get => "/address_details/new").to route_to("address_details#new")
    end

    it "routes to #show" do
      expect(:get => "/address_details/1").to route_to("address_details#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/address_details/1/edit").to route_to("address_details#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/address_details").to route_to("address_details#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/address_details/1").to route_to("address_details#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/address_details/1").to route_to("address_details#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/address_details/1").to route_to("address_details#destroy", :id => "1")
    end

  end
end
