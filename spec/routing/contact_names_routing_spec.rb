require "rails_helper"

RSpec.describe ContactNamesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/contact_names").to route_to("contact_names#index")
    end

    it "routes to #new" do
      expect(:get => "/contact_names/new").to route_to("contact_names#new")
    end

    it "routes to #show" do
      expect(:get => "/contact_names/1").to route_to("contact_names#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/contact_names/1/edit").to route_to("contact_names#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/contact_names").to route_to("contact_names#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/contact_names/1").to route_to("contact_names#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/contact_names/1").to route_to("contact_names#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/contact_names/1").to route_to("contact_names#destroy", :id => "1")
    end

  end
end
