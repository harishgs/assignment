FactoryGirl.define do
  factory :contact_name do
    id "1"
    firstName "test"
    lastName "gs"
    email "test"
    created_at ""
    updated_at ""
  end
  factory :address_detail do
    id "2"
    address_text "test"
    city_name "test"
    state_name "test"
    country_name "test"
    contact_name_id "1"
    created_at ""
    updated_at ""
  end
  factory :phone_number do
    id "2"
    contact_number "33222222"
    contact_name_id "1"
    created_at ""
    updated_at ""
  end
end
