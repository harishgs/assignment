require 'rails_helper'

RSpec.describe "address_details/show", type: :view do
  before(:each) do
    @address_detail = assign(:address_detail, AddressDetail.create!(
      :address1 => "Address1",
      :address2 => "Address2",
      :address3 => "Address3",
      :pincode => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Address1/)
    expect(rendered).to match(/Address2/)
    expect(rendered).to match(/Address3/)
    expect(rendered).to match(/2/)
  end
end
