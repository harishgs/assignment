require 'rails_helper'

RSpec.describe "address_details/edit", type: :view do
  before(:each) do
    @address_detail = assign(:address_detail, AddressDetail.create!(
      :address1 => "MyString",
      :address2 => "MyString",
      :address3 => "MyString",
      :pincode => 1
    ))
  end

  it "renders the edit address_detail form" do
    render

    assert_select "form[action=?][method=?]", address_detail_path(@address_detail), "post" do

      assert_select "input#address_detail_address1[name=?]", "address_detail[address1]"

      assert_select "input#address_detail_address2[name=?]", "address_detail[address2]"

      assert_select "input#address_detail_address3[name=?]", "address_detail[address3]"

      assert_select "input#address_detail_pincode[name=?]", "address_detail[pincode]"
    end
  end
end
