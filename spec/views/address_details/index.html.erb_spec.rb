require 'rails_helper'

RSpec.describe "address_details/index", type: :view do
  before(:each) do
    assign(:address_details, [
      AddressDetail.create!(
        :address1 => "Address1",
        :address2 => "Address2",
        :address3 => "Address3",
        :pincode => 2
      ),
      AddressDetail.create!(
        :address1 => "Address1",
        :address2 => "Address2",
        :address3 => "Address3",
        :pincode => 2
      )
    ])
  end

  it "renders a list of address_details" do
    render
    assert_select "tr>td", :text => "Address1".to_s, :count => 2
    assert_select "tr>td", :text => "Address2".to_s, :count => 2
    assert_select "tr>td", :text => "Address3".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
