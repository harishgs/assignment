require 'rails_helper'

RSpec.describe "contact_names/edit", type: :view do
  before(:each) do
    @contact_name = assign(:contact_name, ContactName.create!(
      :firstName => "",
      :lastName => "",
      :email => "",
    ))
  end

  it "renders the edit contact_name form" do
    render

    assert_select "form[action=?][method=?]", contact_name_path(@contact_name), "post" do

      assert_select "input#contact_name_firstName[name=?]", "contact_name[firstName]"

      assert_select "input#contact_name_60[name=?]", "contact_name[60]"

      assert_select "input#contact_name_lastName[name=?]", "contact_name[lastName]"

      assert_select "input#contact_name_225[name=?]", "contact_name[225]"

      assert_select "input#contact_name_email[name=?]", "contact_name[email]"

      assert_select "input#contact_name_80[name=?]", "contact_name[80]"
    end
  end
end
