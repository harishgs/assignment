require 'rails_helper'

RSpec.describe "contact_names/new", type: :view do
  before(:each) do
    assign(:contact_name, ContactName.new(
      :firstName => "",
      :lastName => "",
      :email => "",
    ))
  end

  it "renders new contact_name form" do
    render

    assert_select "form[action=?][method=?]", contact_names_path, "post" do

      assert_select "input#contact_name_firstName[name=?]", "contact_name[firstName]"

      assert_select "input#contact_name_lastName[name=?]", "contact_name[lastName]"

      assert_select "input#contact_name_email[name=?]", "contact_name[email]"

    end
  end
end
