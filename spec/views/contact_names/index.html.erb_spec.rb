require 'rails_helper'

RSpec.describe "contact_names/index", type: :view do
  before(:each) do
    assign(:contact_names, [
      ContactName.create!(
        :firstName => "",
        :lastName => "",
        :email => "",
      ),
      ContactName.create!(
        :firstName => "",
        :lastName => "",
        :email => "",
      )
    ])
  end

  it "renders a list of contact_names" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
