require 'rails_helper'

RSpec.describe "phone_numbers/index", type: :view do
  before(:each) do
    assign(:phone_numbers, [
      PhoneNumber.create!(
        :contact_number => 2
      ),
      PhoneNumber.create!(
        :contact_number => 2
      )
    ])
  end

  it "renders a list of phone_numbers" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
