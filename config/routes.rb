Rails.application.routes.draw do
  #devise_for :models
  devise_for :users
  resources :phone_numbers
  resources :address_details
  resources :contact_names
  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end